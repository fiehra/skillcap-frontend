import app from '../src/app';
import mongoose from 'mongoose';
import request from 'supertest';
import User from '../src/models/user';
import jwt from 'jsonwebtoken';
import VerifyToken from '../src/models/verifyToken';

describe('verificationModule', () => {
  jest.mock('../src/helper/email.helper');

  const url = process.env.MONGO + '';

  const userId = new mongoose.Types.ObjectId();
  const user2Id = new mongoose.Types.ObjectId();
  const userOne = {
    _id: userId,
    email: 'user@controller.com',
    verified: true,
    username: 'user',
    password: 'password',
    tokens: [
      {
        token: jwt.sign(
          {
            userId: userId,
            email: 'user@controller.com',
            username: 'user',
          },
          process.env.JWT_SECRET,
        ),
      },
    ],
  };

  const expiredToken = {
    userId: user2Id,
    token: 'expiredToken',
    expireAt: Date.now() - 1000,
  };

  beforeAll(async () => {
    await mongoose.disconnect();
    await mongoose.connect(url, {});
    await User.deleteMany();
    await VerifyToken.deleteMany();
    await new User(userOne).save();
  });

  beforeEach(async () => {});

  afterAll(async () => {
    await VerifyToken.deleteMany();
    await User.deleteMany();
    await mongoose.disconnect();
  });

  // test('verifyToken 500 verifying failed', async () => {
  //   await process.nextTick(() => {});
  //   const verifyResonse = await request(app).get('/api/verification/verify/123').expect(500);
  //   expect(verifyResonse.body.message).toBe('invalid token');
  // });

  test('verifyToken 404 verifying failed expired token', async () => {
    await new VerifyToken(expiredToken).save();
    const verifyResonse = await request(app)
      .get('/api/verification/verify/expiredToken')
      .expect(404);
    expect(verifyResonse.body.message).toBe('invalid token');
  });

  test('verifyToken success', async () => {
    const userCountBefore = await User.countDocuments();
    const signupResponse = await request(app)
      .post('/api/auth/signup')
      .send({
        email: 'fiehra@gsg.com',
        username: 'fiehra',
        password: 'fiehra',
      })
      .expect(201);

    const token = signupResponse.body.token;
    expect(userCountBefore + 1).toBe(await User.countDocuments());
    expect(await VerifyToken.countDocuments()).toBe(2);

    await request(app)
      .get('/api/verification/verify/' + token)
      .expect(302);
    expect(await VerifyToken.countDocuments()).toBe(0);
    const verified = await User.findOne({ username: 'fiehra' }).select('verified');
    expect(verified?.verified).toBe(true);
  });

  test('resendVerificationLink already verified', async () => {
    const resendResponse = await request(app)
      .post('/api/verification/resend')
      .send({
        email: 'fiehra@gsg.com',
      })
      .expect(400);
    expect(resendResponse.body.message).toBe('user already verified');
  });

  test('resendVerificationLink success', async () => {
    const userCountBefore = await User.countDocuments();
    await request(app)
      .post('/api/auth/signup')
      .send({
        email: 'nurias@gsg.com',
        username: 'nurias',
        password: 'nurias',
      })
      .expect(201);

    expect(userCountBefore + 1).toBe(await User.countDocuments());
    expect(await VerifyToken.countDocuments()).toBe(1);

    const verified = await User.findOne({ username: 'nurias' }).select('verified');
    expect(verified?.verified).toBe(false);

    const resendResponse = await request(app)
      .post('/api/verification/resend')
      .send({
        email: 'nurias@gsg.com',
      })
      .expect(201);
    expect(resendResponse.body.message).toBe('email sent');
  });
});
