import { autoInjectable } from 'tsyringe';
import { VerifyTokenInterface } from '../models/verifyToken';
import sgMail from '@sendgrid/mail';
sgMail.setApiKey(process.env.SENDGRID_API_KEY!);

@autoInjectable()
export class EmailHelper {
  constructor() {}

  createVerificationEmail(receiverEmail: string, verificationLink: string) {
    const message = {
      to: receiverEmail, // Change to your recipient
      from: process.env.EMAIL!, // Change to your verified sender
      subject: 'verify email address',
      text: `verification link: ${verificationLink}`,
      html: `<strong>skillcap.org</strong> verification link: ${verificationLink}`,
    };
    return message;
  }

  createLoginNotificationEmail(
    receiverEmail: string,
    ip: string,
    userAgent: string = 'unknown',
  ) {
    const now = new Date().toLocaleString();
    const message = {
      to: receiverEmail, // Change to your recipient
      from: process.env.EMAIL!, // Change to your verified sender
      subject: 'skillcap.org new login detected',
      text: 'new login detected',
      html: `<strong>skillcap.org</strong><br><p>new login detected <br> from: ${userAgent}<br> at: ${now}<br> from ip: ${ip}</p>`,
    };
    return message;
  }

  async sendVerificationEmail(
    receiverEmail: string,
    verification: VerifyTokenInterface,
    hostUrl: string,
  ) {
    try {
      const verificationUrl = `${hostUrl}/api/verification/verify/${verification.token}`;
      const verificationEmail = this.createVerificationEmail(
        receiverEmail,
        verificationUrl,
      );
      let res = await sgMail.send(verificationEmail);
      if (res[0].statusCode === 202) {
        return {
          statusCode: 202,
          message: 'email sent',
        };
      }
    } catch (error) {
      console.log(error.response.body.errors);
    }
  }

  async sendLoginNotificationEmail(
    receiverEmail: string,
    ip: string = 'unknown ip',
    userAgent?: string,
  ) {
    try {
      const loginNotificationEmail = this.createLoginNotificationEmail(
        receiverEmail,
        ip,
        userAgent,
      );
      let res = await sgMail.send(loginNotificationEmail);
      if (res[0].statusCode === 202) {
        return {
          statusCode: 202,
          message: 'email sent',
        };
      }
    } catch (error) {
      console.log(error.response.body.errors);
    }
  }
}
