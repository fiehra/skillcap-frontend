import multer from 'multer';
import { Request } from 'express';

const MIME_TYPE_MAP = {
  'image/png': 'png',
  'image/jpeg': 'jpg',
  'image/jpg': 'jpg',
};

type DestinationCallback = (error: Error | null, destination: string) => void;
type FileNameCallback = (error: Error | null, filename: string) => void;

export const fileStorage = multer.diskStorage({
  destination: (
    request: Request,
    file: Express.Multer.File,
    callback: DestinationCallback,
  ): void => {
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error: Error | null = new Error('invalid mime type');
    if (isValid) {
      error = null;
    }
    callback(error, 'images');
  },

  filename: (req: Request, file: Express.Multer.File, callback: FileNameCallback): void => {
    const name = file.originalname.toLowerCase().split(' ').join('-');
    const ext = MIME_TYPE_MAP[file.mimetype];
    if (name.includes('.' + ext)) {
      callback(null, name);
    } else {
      callback(null, name + '.' + ext);
    }
  },
});
