import { Request, Response } from 'express';
import { UserService } from './user.service';
import { autoInjectable, container } from 'tsyringe';

@autoInjectable()
export class UserController {
  constructor() {}

  signup(req: Request, res: Response, next) {
    container.resolve(UserService).signupUser(req, res);
  }

  login(req: Request, res: Response, next) {
    container.resolve(UserService).loginUser(req, res);
  }
  refreshToken(req: Request, res: Response, next) {
    container.resolve(UserService).refreshToken(req, res);
  }
}
