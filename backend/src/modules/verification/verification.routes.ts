import express from 'express';
import { VerificationController } from './verification.controller';
import { autoInjectable } from 'tsyringe';

@autoInjectable()
export class VerificationRoutes {
  router = express.Router();

  constructor(private verificationController: VerificationController) {
    this.configureRoutes();
  }

  configureRoutes() {
    this.router.get('/verification/verify/:token', this.verificationController.verify);
    this.router.post('/verification/resend', this.verificationController.resendLink);
  }
}
