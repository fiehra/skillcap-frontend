## skillcap.org

rewrite of skillcap.org


## frontend
# development
1. clone the repo
2. yarn install
3. run yarn dev to start frontend 
4. open localhost:4200 in your browser

# testing
1. yarn test

## backend
# development
1. clone project <br>
   https: git clone https://gitlab.com/fiehra/skillcap-backend.git<br>
   ssh: git@gitlab.com:fiehra/skillcap-backend.git<br>
2. yarn install  
3. add dev.env in src directory 

# xxx.env template:
PORT=<XXXX> <br>
MONGO=<mongo-connection> <br>
JWT_SECRET=<yourJWTSecretKey> <br>
NODE_ENV=<nodeEnvironment> <br>
EMAIL=<senderEmail> <br>
SENDGRID_API_KEY=<sendgridApiKey> <br>

4. yarn dev to start backend

# testing
1. add test.env
2. yarn test


## license
MIT
