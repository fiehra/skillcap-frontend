export const environment = {
  production: false,
  authRedirectUrl: 'http://localhost:4200/',
  backendUrl: 'http://localhost:3333',
};
