export class Image {
  constructor(
    public id?: string | null,
    public filename?: string | null,
    public mobileUrl?: string | null,
    public desktopUrl?: string | null,
    public alt?: string | null,
    public word?: string | null,
  ) {}
}
