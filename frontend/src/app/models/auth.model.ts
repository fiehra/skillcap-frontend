export class AuthData {
  constructor(
    public userId: string | null,
    public email: string | null,
    public username: string | null,
    public role: string | null,
    public token: string | null,
    public expirationDate: string | null,
  ) {}
}
