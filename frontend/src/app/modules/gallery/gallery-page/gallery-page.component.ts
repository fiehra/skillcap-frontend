import { Component } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { LightboxGalleryComponent } from 'src/app/core/components/lightbox-gallery/lightbox-gallery.component';
import { ScrollIndicatorComponent } from 'src/app/core/components/scroll-indicator/scroll-indicator.component';

@Component({
  selector: 'gallery-page',
  standalone: true,
  imports: [
    LightboxGalleryComponent,
    TranslateModule,
    ScrollIndicatorComponent
  ],
  templateUrl: './gallery-page.component.html',
  styleUrl: './gallery-page.component.scss'
})
export class GalleryPageComponent {
  images;

  ngOnInit() {
    this.images = [];
    //for (let image of this.projectReport.images) {
    //  let url =
    //    environment.backendUrl + '/project/image/' + encodeURI(image.imageFileName) + '/2000/1000';
    //  let caption = this.textHelper.getTextForLanguage(
    //    image.description,
    //    this.translateService.currentLang,
    //  );
    //  this.images.unshift({ imageUrl: url, caption });
    //}

  }

}
