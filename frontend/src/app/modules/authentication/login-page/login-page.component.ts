import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { SnackbarHelper } from 'src/app/core/helper/snackbar.helper';
import { Store } from '@ngrx/store';
import { Router, RouterLink } from '@angular/router';
import { login, selectIsAuthenticated } from 'src/app/store/authentication.store';
import { AppState } from 'src/app/store/app.state';
import { SkillcapInputComponent } from 'src/app/core/components/skillcap-input/skillcap-input.component';
import { NgIf } from '@angular/common';
import { FormControlPipe } from 'src/app/core/pipes/formControl.pipe';
import { SsrHelper } from 'src/app/core/helper/ssr.helper';

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  standalone: true,
  imports: [
    NgIf,
    SkillcapInputComponent,
    RouterLink,
    FormControlPipe,
    TranslateModule,
  ],
})
export class LoginPageComponent implements OnInit, OnDestroy {
  isAuthenticated$ = this.store.select(selectIsAuthenticated);

  loginForm = new FormGroup({
    username: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required],
    }),
    password: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required],
    }),
  });

  constructor(
    private snackbarHelper: SnackbarHelper,
    private translateService: TranslateService,
    private router: Router,
    private ssrHelper: SsrHelper,
    private store: Store<AppState>,
  ) {}

  ngOnInit() {
    this.ssrHelper.scrollTop()
    this.store.select('authenticationState').subscribe((state) => {
      if (state.error) {
        this.loginForm.reset();
      }
      if (state.isAuthenticated) {
        this.router.navigate(['/']);
      }
    });
  }

  login() {
    if (this.loginForm.valid) {
      const username = this.loginForm.get('username')?.getRawValue();
      const password = this.loginForm.get('password')?.getRawValue();
      this.store.dispatch(login({ username: username, password: password }));
    } else {
      const messageInvalid = this.translateService.instant('snackInvalidForm');
      this.snackbarHelper.openSnackBar(messageInvalid);
      this.loginForm.get('username')?.markAsTouched();
      this.loginForm.get('password')?.markAsTouched();
    }
  }

  ngOnDestroy() {}
}
