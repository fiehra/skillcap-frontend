import { provideHttpClientTesting } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { LoginPageComponent } from './login-page.component';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { SnackbarHelper } from 'src/app/core/helper/snackbar.helper';
import { RouterTestingModule } from '@angular/router/testing';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';

window.scrollTo = jest.fn();

describe('LoginPageComponent', () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;
  let store: MockStore;
  let snackbarHelper: SnackbarHelper;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot({}),
        NoopAnimationsModule,
        LoginPageComponent],
      providers: [
        provideMockStore({
          initialState: {
            authentication: {
              isAuthenticated: false,
              error: null,
            },
          },
        }),
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(LoginPageComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);
    snackbarHelper = TestBed.inject(SnackbarHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    const errorState = {
      isAuthenticated: false,
      error: 'error',
    };
    jest.spyOn(store, 'select').mockReturnValue(of(errorState));
    jest.spyOn(component.loginForm, 'reset');

    component.ngOnInit();
    fixture.detectChanges();
    expect(store.select).toHaveBeenCalled();
    expect(component.loginForm.reset).toHaveBeenCalled();
  });

  it('login form valid', () => {
    jest.spyOn(store, 'dispatch');
    component.loginForm.get('username').setValue('username@email');
    component.loginForm.get('password').setValue('password');

    component.login();
    fixture.detectChanges();
    expect(store.dispatch).toHaveBeenCalled();
  });

  it('login form invalid', () => {
    jest.spyOn(snackbarHelper, 'openSnackBar').mockImplementation();
    jest.spyOn(component.loginForm, 'markAsTouched').mockImplementation();
    component.loginForm.get('username').setValue('');
    component.loginForm.get('password').setValue('password');

    component.login();
    fixture.detectChanges();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(component.loginForm.markAsTouched).toHaveBeenCalled();
  });
});
