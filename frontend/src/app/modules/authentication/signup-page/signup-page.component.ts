import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { FormHelper } from 'src/app/core/helper/form.helper';
import { SnackbarHelper } from 'src/app/core/helper/snackbar.helper';
import { Store } from '@ngrx/store';
import { selectIsLoading, signup } from 'src/app/store/authentication.store';
import { AppState } from 'src/app/store/app.state';
import { RouterLink } from '@angular/router';
import { SkillcapInputComponent } from 'src/app/core/components/skillcap-input/skillcap-input.component';
import { NgIf, AsyncPipe } from '@angular/common';
import { FormControlPipe } from 'src/app/core/pipes/formControl.pipe';
import { SsrHelper } from 'src/app/core/helper/ssr.helper';

@Component({
  selector: 'signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss'],
  standalone: true,
  imports: [
    NgIf,
    SkillcapInputComponent,
    RouterLink,
    AsyncPipe,
    FormControlPipe,
    TranslateModule,
  ],
})
export class SignupPageComponent implements OnInit {
  isLoading$ = this.store.select(selectIsLoading);
  authenticationSub!: Subscription;

  public signupForm: FormGroup = new FormGroup({
    email: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required, Validators.email],
    }),
    username: new FormControl('', {
      nonNullable: true,
      validators: Validators.required,
    }),
    password: new FormControl('', {
      nonNullable: true,
      validators: Validators.required,
    }),
  });

  constructor(
    private snackbarHelper: SnackbarHelper,
    private translateService: TranslateService,
    private formHelper: FormHelper,
    private ssrHelper: SsrHelper,
    private store: Store<AppState>,
  ) {}

  ngOnInit() {
    this.ssrHelper.scrollTop()
    this.authenticationSub = this.store
      .select('authenticationState')
      .subscribe((state) => {
        if (state.error) {
          this.signupForm.reset();
        }
      });
    this.signupForm.addControl(
      'passwordConfirmation',
      new FormControl('', { validators: [Validators.required] }),
    );
    this.signupForm.setValidators(this.formHelper.checkPasswords);
  }

  signUp() {
    if (this.signupForm.valid) {
      const email = this.signupForm.get('email')?.getRawValue();
      const username = this.signupForm.get('username')?.getRawValue();
      const password = this.signupForm.get('password')?.getRawValue();
      this.store.dispatch(
        signup({
          email: email,
          username: username,
          password: password,
        }),
      );
    } else {
      const messageInvalid = this.translateService.instant('snackInvalidForm');
      this.snackbarHelper.openSnackBar(messageInvalid);
      this.signupForm.get('email')?.markAsTouched();
      this.signupForm.get('username')?.markAsTouched();
      this.signupForm.get('password')?.markAsTouched();
    }
  }

  ngOnDestroy() {
    this.authenticationSub?.unsubscribe();
  }
}
