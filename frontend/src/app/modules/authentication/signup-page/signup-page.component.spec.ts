import { provideHttpClientTesting } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { SignupPageComponent } from './signup-page.component';
import { SnackbarHelper } from 'src/app/core/helper/snackbar.helper';
import { of } from 'rxjs';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { provideRouter } from '@angular/router';
import { routes } from 'src/app/app.routes';

window.scrollTo = jest.fn();

describe('SignupPageComponent', () => {
  let component: SignupPageComponent;
  let fixture: ComponentFixture<SignupPageComponent>;
  let store: MockStore;
  let snackbarHelper: SnackbarHelper;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot({}),
        NoopAnimationsModule, SignupPageComponent],
      providers: [
        provideRouter(routes),
        provideMockStore({
          initialState: {
            authentication: {
              isAuthenticated: false,
              error: null,
            },
          },
        }),
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(SignupPageComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);
    snackbarHelper = TestBed.inject(SnackbarHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    const errorState = {
      isAuthenticated: false,
      error: 'error',
    };
    jest.spyOn(store, 'select').mockReturnValue(of(errorState));
    jest.spyOn(component.signupForm, 'reset');

    component.ngOnInit();
    fixture.detectChanges();
    expect(store.select).toHaveBeenCalled();
    expect(component.signupForm.reset).toHaveBeenCalled();
  });

  it('signUp form valid', () => {
    jest.spyOn(store, 'dispatch');
    component.signupForm.get('email').setValue('email@email.com');
    component.signupForm.get('username').setValue('username@email');
    component.signupForm.get('password').setValue('password');
    component.signupForm.get('passwordConfirmation').setValue('password');

    component.signUp();
    fixture.detectChanges();
    expect(store.dispatch).toHaveBeenCalled();
  });

  it('signUp form invalid', () => {
    jest.spyOn(snackbarHelper, 'openSnackBar').mockImplementation();
    jest.spyOn(component.signupForm, 'markAsTouched').mockImplementation();
    component.signupForm.get('username').setValue('');
    component.signupForm.get('password').setValue('password');

    component.signUp();
    fixture.detectChanges();
    expect(snackbarHelper.openSnackBar).toHaveBeenCalled();
    expect(component.signupForm.markAsTouched).toHaveBeenCalled();
  });
});
