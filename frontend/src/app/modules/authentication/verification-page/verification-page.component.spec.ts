import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationPageComponent } from './verification-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

describe('VerificationPageComponent', () => {
  let component: VerificationPageComponent;
  let store: MockStore;

  let fixture: ComponentFixture<VerificationPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    providers: [
        provideMockStore({
            initialState: {
                authentication: {
                    isAuthenticated: false,
                    error: null,
                },
            },
        }),
    ],
    imports: [TranslateModule.forRoot({}), VerificationPageComponent],
}).compileComponents();
    fixture = TestBed.createComponent(VerificationPageComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    const stateMock = {
      emailFromSignup: 'false@email.com',
    };
    jest.spyOn(store, 'select').mockReturnValue(of(stateMock));
    component.ngOnInit();
    fixture.detectChanges();
    expect(store.select).toHaveBeenCalled();
    expect(component.email).toBe('false@email.com');
  });

  it('resendLink', () => {
    jest.spyOn(store, 'dispatch');
    component.resendLink();
    fixture.detectChanges();
    expect(store.dispatch).toHaveBeenCalled();
  });
});
