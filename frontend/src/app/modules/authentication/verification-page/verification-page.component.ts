import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { take } from 'rxjs';
import { AppState } from 'src/app/store/app.state';
import { resendLink } from 'src/app/store/authentication.store';
import { TranslateModule } from '@ngx-translate/core';

@Component({
    selector: 'verification-page',
    templateUrl: './verification-page.component.html',
    styleUrls: ['./verification-page.component.scss'],
    standalone: true,
    imports: [TranslateModule],
})
export class VerificationPageComponent implements OnInit {
  email: string;
  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store
      .select('authenticationState')
      .pipe(take(1))
      .subscribe((state) => (this.email = state.emailFromSignup));
  }

  resendLink() {
    this.store.dispatch(
      resendLink({
        email: this.email,
      }),
    );
  }
}
