import { Component, OnInit } from '@angular/core';
import { ProjectsSectionComponent } from '../components/projects-section/projects-section.component';
import { LandingComponent } from '../components/landing/landing.component';
import { SsrHelper } from 'src/app/core/helper/ssr.helper';

@Component({
  selector: 'home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  standalone: true,
  imports: [
    LandingComponent,
    ProjectsSectionComponent
  ],
})
export class HomePageComponent implements OnInit {

  constructor(private ssrHelper: SsrHelper) {}

  ngOnInit(): void {
    this.ssrHelper.scrollTop()
  }
}
