import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { LandingComponent } from '../components/landing/landing.component';
import { ProjectsSectionComponent } from '../components/projects-section/projects-section.component';
import { HomePageComponent } from './home-page.component';
import { provideMockStore } from '@ngrx/store/testing';
import { ActivatedRoute } from '@angular/router';

window.scrollTo = jest.fn();

describe('HomePageComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        provideMockStore({}),
        {
          provide: ActivatedRoute,
          useValue: { }
        },
      ],
      imports: [
        TranslateModule.forRoot({}),
        HomePageComponent,
        LandingComponent,
        ProjectsSectionComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
