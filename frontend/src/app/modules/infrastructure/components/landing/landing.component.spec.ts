import { provideHttpClientTesting } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { LandingComponent } from './landing.component';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { provideMockStore } from '@ngrx/store/testing';

describe('LandingComponent', () => {
  let component: LandingComponent;
  let fixture: ComponentFixture<LandingComponent>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot({}),
        LandingComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { }
        },
        provideHttpClient(withInterceptorsFromDi()),
        provideMockStore({}),
        provideHttpClientTesting()
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(LandingComponent);
    router = TestBed.inject(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('showMoreScroll', () => {
    jest.spyOn(component.scrollClicked, 'emit').mockImplementation();
    jest.spyOn(router, 'navigate').mockImplementation();
    component.showMoreScroll();
    fixture.detectChanges();
    expect(component.scrollClicked.emit).toHaveBeenCalled();
    expect(component.scrollClicked.emit).toHaveBeenCalled();
  });
});
