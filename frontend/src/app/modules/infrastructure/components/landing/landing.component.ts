import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectIsAuthenticated } from 'src/app/store/authentication.store';
import { AppState } from 'src/app/store/app.state';
import { TranslateModule } from '@ngx-translate/core';
import { RouterLink } from '@angular/router';
import { NgIf, AsyncPipe } from '@angular/common';

@Component({
    selector: 'landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss'],
    standalone: true,
    imports: [
        NgIf,
        RouterLink,
        TranslateModule,
        AsyncPipe,
    ],
})
export class LandingComponent implements OnInit {
  isAuthenticated$ = this.store.select(selectIsAuthenticated);
  @Output() scrollClicked = new EventEmitter();

  constructor(private store: Store<AppState>) {}

  ngOnInit() {}

  showMoreScroll() {
    this.scrollClicked.emit();
  }
}
