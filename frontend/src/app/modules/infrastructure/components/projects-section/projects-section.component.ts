import { Component, OnInit } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RouterLink } from '@angular/router';

@Component({
    selector: 'projects-section',
    templateUrl: './projects-section.component.html',
    styleUrls: ['./projects-section.component.scss'],
    standalone: true,
    imports: [RouterLink, TranslateModule]
})
export class ProjectsSectionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
