import { Component, OnInit } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { ScrollIndicatorComponent } from 'src/app/core/components/scroll-indicator/scroll-indicator.component';
import { SsrHelper } from 'src/app/core/helper/ssr.helper';

@Component({
    selector: 'privacy-page',
    templateUrl: './privacy-page.component.html',
    styleUrls: ['./privacy-page.component.scss'],
    standalone: true,
    imports: [ScrollIndicatorComponent, TranslateModule],
})
export class PrivacyPageComponent implements OnInit {
  constructor(private ssrHelper: SsrHelper) {}

  ngOnInit(): void {
    this.ssrHelper.scrollTop()
  }
}
