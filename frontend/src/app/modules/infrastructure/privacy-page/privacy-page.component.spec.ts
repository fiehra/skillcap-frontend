import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { PrivacyPageComponent } from './privacy-page.component';
import { ActivatedRoute, provideRouter } from '@angular/router';
import { ScrollIndicatorComponent } from 'src/app/core/components/scroll-indicator/scroll-indicator.component';
import { routes } from 'src/app/app.routes';

window.scrollTo = jest.fn();

describe('PrivacyPageComponent', () => {
  let component: PrivacyPageComponent;
  let fixture: ComponentFixture<PrivacyPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot({}),
        PrivacyPageComponent,
        ScrollIndicatorComponent
      ],
      providers: [
        provideRouter(routes),
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(PrivacyPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
