import { Component, OnInit } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SsrHelper } from 'src/app/core/helper/ssr.helper';

@Component({
    selector: 'imprint-page',
    templateUrl: './imprint-page.component.html',
    styleUrls: ['./imprint-page.component.scss'],
    standalone: true,
    imports: [TranslateModule],
})
export class ImprintPageComponent implements OnInit {
  constructor(private ssrHelper: SsrHelper) {}

  ngOnInit(): void {
    this.ssrHelper.scrollTop()
  }
}
