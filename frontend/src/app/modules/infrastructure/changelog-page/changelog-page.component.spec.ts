import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ChangelogPageComponent } from './changelog-page.component';
import { provideRouter } from '@angular/router';
import { routes } from 'src/app/app.routes';

window.scrollTo = jest.fn();

describe('ChangelogPageComponent', () => {
  let component: ChangelogPageComponent;
  let fixture: ComponentFixture<ChangelogPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot({}),
        ChangelogPageComponent,
      ],
      providers: [
        provideRouter(routes),
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ChangelogPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
