import { Component, OnInit } from '@angular/core';
import { VersionHelper } from 'src/app/core/helper/version.helper';
import { TranslateModule } from '@ngx-translate/core';
import { ScrollIndicatorComponent } from 'src/app/core/components/scroll-indicator/scroll-indicator.component';
import { SsrHelper } from 'src/app/core/helper/ssr.helper';

@Component({
    selector: 'changelog-page',
    templateUrl: './changelog-page.component.html',
    styleUrls: ['./changelog-page.component.scss'],
    standalone: true,
    imports: [ScrollIndicatorComponent, TranslateModule],
})
export class ChangelogPageComponent implements OnInit {
  version: string;

  constructor(private ssrHelper: SsrHelper, private versionHelper: VersionHelper) {}

  ngOnInit(): void {
    this.version = this.versionHelper.getVersion();
    this.ssrHelper.scrollTop()
  }
}
