import { Routes } from '@angular/router';
import { HomePageComponent } from './modules/infrastructure/home-page/home-page.component';
import { ChangelogPageComponent } from './modules/infrastructure/changelog-page/changelog-page.component';
import { ImprintPageComponent } from './modules/infrastructure/imprint-page/imprint-page.component';
import { PrivacyPageComponent } from './modules/infrastructure/privacy-page/privacy-page.component';
import { NotFoundPageComponent } from './modules/infrastructure/not-found-page/not-found-page.component';
import { ProfilePageComponent } from './modules/profile/profile-page/profile-page.component';
import { SignupPageComponent } from './modules/authentication/signup-page/signup-page.component';
import { LoginPageComponent } from './modules/authentication/login-page/login-page.component';
import { VerificationPageComponent } from './modules/authentication/verification-page/verification-page.component';
import { GalleryPageComponent } from './modules/gallery/gallery-page/gallery-page.component';

export const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'home', component: HomePageComponent },
  { path: 'changelog', component: ChangelogPageComponent },
  { path: 'imprint', component: ImprintPageComponent },
  { path: 'privacy', component: PrivacyPageComponent },
  { path: 'profile', component: ProfilePageComponent },
  { path: 'signup', component: SignupPageComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'verify', component: VerificationPageComponent },
  { path: 'gallery', component: GalleryPageComponent },
  { path: 'games', component: GalleryPageComponent },
  { path: '**', component: NotFoundPageComponent },
];
