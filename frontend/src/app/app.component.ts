import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from './services/authentication.service';
import { FooterComponent } from './core/components/footer/footer.component';
import { RouterOutlet } from '@angular/router';
import { MainMenuComponent } from './core/components/main-menu/main-menu.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  standalone: true,
  imports: [
    MainMenuComponent,
    RouterOutlet,
    FooterComponent,
  ],
})
export class AppComponent implements OnInit {

  constructor(
    private authService: AuthenticationService,
    private translateService: TranslateService,
  ) {
    this.translateService.setDefaultLang('en');
    this.translateService.use('en');
  }

  ngOnInit() {
    this.authService.autoLogin();
  }
}
