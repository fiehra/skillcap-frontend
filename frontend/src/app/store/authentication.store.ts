import {
  createAction,
  createReducer,
  createSelector,
  on,
  props,
} from '@ngrx/store';
import { AppState } from './app.state';

export const signup = createAction(
  '[Authentication] sign up',
  props<{
    email: string;
    username: string;
    password: string;
  }>()
);
export const signupSuccess = createAction(
  '[Authentication] sign up success',
  props<{
    email: string;
  }>()
);
export const signupFailure = createAction(
  '[Authentication] sign up failure',
  props<{
    error: string;
  }>()
);
export const login = createAction(
  '[Authentication] login',
  props<{
    username: string;
    password: string;
  }>()
);
export const loginSuccess = createAction(
  '[Authentication] login success',
  props<{
    token: string;
    username: string;
    userId: string;
    expirationDate: string;
  }>()
);
export const loginFailure = createAction(
  '[Authentication] login failure',
  props<{
    error: string;
  }>()
);
export const logout = createAction('[Authentication] logout');
export const refreshLogin = createAction(
  '[Authentication] refresh login',
  props<{
    username: string;
    expirationDate: string;
  }>()
);
export const refreshLoginSuccess = createAction(
  '[Authentication] refresh login success',
  props<{
    token: string;
    username: string;
    userId: string;
    expirationDate: string;
  }>()
);
export const resendLink = createAction(
  '[Authentication] resend link',
  props<{
    email: string;
  }>()
);

export interface AuthState {
  isAuthenticated: boolean;
  token: string | null;
  username: string | null;
  userId: string | null;
  expirationDate: string | null;
  emailFromSignup: string | null;
  isLoading: boolean;
  error: string | null;
}
const initialState: AuthState = {
  isAuthenticated: false,
  token: null,
  username: null,
  userId: null,
  expirationDate: null,
  emailFromSignup: null,
  isLoading: false,
  error: null,
};

export const authenticationFeature = (state: AppState) =>
  state.authenticationState;

export const authenticationReducer = createReducer(
  initialState,
  on(signup, (state) => ({
    ...state,
    isLoading: true,
    error: null,
  })),
  on(signupSuccess, (state, { email }) => ({
    ...state,
    isLoading: false,
    emailFromSignup: email,
    error: null,
  })),
  on(signupFailure, (state, { error }) => ({
    ...state,
    isLoading: false,
    isAuthenticated: false,
    error: error,
  })),
  on(login, (state) => ({
    ...state,
    isLoading: true,
    error: null,
  })),
  on(loginSuccess, (state, { token, username, userId, expirationDate }) => ({
    ...state,
    isAuthenticated: true,
    token: token,
    username: username,
    userId: userId,
    expirationDate: expirationDate,
    emailFromSignup: null,
    isLoading: false,
    error: null,
  })),
  on(loginFailure, (state, { error }) => ({
    ...state,
    isAuthenticated: false,
    isLoading: false,
    error: error,
  })),
  on(logout, (state) => ({
    ...state,
    isAuthenticated: false,
    token: null,
    userId: null,
    email: null,
    role: null,
    expirationDate: null,
  })),
  on(
    refreshLoginSuccess,
    (state, { token, username, userId, expirationDate }) => ({
      ...state,
      isAuthenticated: true,
      token: token,
      username: username,
      userId: userId,
      expirationDate: expirationDate,
      emailFromSignup: null,
      isLoading: false,
      error: null,
    })
  ),
  on(resendLink, (state) => ({
    ...state,
    isLoading: true,
    error: null,
  }))
);

export const selectIsLoading = createSelector(
  authenticationFeature,
  (state: AuthState) => state.isLoading
);
export const selectIsAuthenticated = createSelector(
  authenticationFeature,
  (state: AuthState) => state.isAuthenticated
);
export const selectToken = createSelector(
  authenticationFeature,
  (state: AuthState) => state.token
);
export const selectUsername = createSelector(
  authenticationFeature,
  (state: AuthState) => state.username
);
export const selectUserId = createSelector(
  authenticationFeature,
  (state: AuthState) => state.userId
);
//export const selectEmail = createSelector(
//  authenticationFeature,
//  (state: AuthState) => state.email,
//);
//export const selectRole = createSelector(
//  authenticationFeature,
//  (state: AuthState) => state.role,
//);
export const selectExpirationDate = createSelector(
  authenticationFeature,
  (state: AuthState) => state.expirationDate
);
