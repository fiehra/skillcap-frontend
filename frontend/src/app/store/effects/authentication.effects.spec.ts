import { TestScheduler } from 'rxjs/testing';

describe('authEffects', () => {
  let testScheduler: TestScheduler;

  beforeEach(() => {
    testScheduler = new TestScheduler(
      (actual, expected) => {
        expect(actual).toEqual(
          expected,
        );
      },
    );
  });

  it('signupsuccess', () => {
    const number = 1;
    const result = 1 - number;
    expect(result).toBe(0);
  });
});
