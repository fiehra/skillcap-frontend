import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { SnackbarHelper } from 'src/app/core/helper/snackbar.helper';
import { TranslateService } from '@ngx-translate/core';
import {
  loginFailure,
  loginSuccess,
  login,
  signup,
  signupFailure,
  signupSuccess,
  refreshLogin,
  refreshLoginSuccess,
  resendLink,
  logout,
} from '../authentication.store';

@Injectable()
export class AuthenticationEffects {
  constructor(
    private actions$: Actions,
    private authService: AuthenticationService,
    private router: Router,
    public snackbarHelper: SnackbarHelper,
    private translate: TranslateService,
  ) {}

  signUp$ = createEffect(() =>
    this.actions$.pipe(
      ofType(signup),
      switchMap((payload) => {
        return this.authService
          .signUp(payload.email, payload.username, payload.password)
          .pipe(
            map(() => {
              return signupSuccess({
                email: payload.email,
              });
            }),
            catchError((error) => {
              console.log(error.error.localizedMessage);
              return of(
                signupFailure({
                  error: error.error.localizedMessage,
                }),
              );
            }),
          );
      }),
    ),
  );

  signUpSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(signupSuccess),
        tap((payload) => {
          this.snackbarHelper.openSnackBar(this.translate.instant('signUpSuccess'));
          this.router.navigateByUrl('/verify');
        }),
      ),
    { dispatch: false },
  );

  signUpFailure$ = createEffect(
    // remove tap   ----------------- to do ---------------
    () =>
      this.actions$.pipe(
        ofType(signupFailure),
        tap((payload) => {
          // console.log(action.payload.error);
        }),
      ),
    { dispatch: false },
  );

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(login),
      switchMap((payload) => {
        return this.authService.login(payload.username, payload.password).pipe(
          map((response) => {
            return loginSuccess(response.authData);
          }),
          catchError((error) => {
            return of(loginFailure({ error: error.error.error }));
          }),
        );
      }),
    ),
  );

  LoginSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loginSuccess),
        tap((payload) => {
          this.authService.saveAuthData(
            payload.token,
            payload.username,
            payload.userId,
            payload.expirationDate,
          );
          this.snackbarHelper.openSnackBar(this.translate.instant('loginSuccess'));
          this.authService.autoLogout();
          this.router.navigateByUrl('/');
        }),
      ),
    { dispatch: false },
  );

  RefreshLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(refreshLogin),
      switchMap((payload) => {
        return this.authService
          .refreshToken(payload.username, payload.expirationDate)
          .pipe(
            map((response) => {
              return refreshLoginSuccess({
                token: response.authData.token,
                username: response.authData.username,
                userId: response.authData.userId,
                expirationDate: response.authData.expirationDate,
              });
            }),
          );
      }),
    ),
  );

  RefreshLoginSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(refreshLoginSuccess),
        tap((payload) => {
          this.authService.saveAuthData(
            payload.token,
            payload.username,
            payload.userId,
            payload.expirationDate,
          );
          this.authService.autoLogout();
        }),
      ),
    { dispatch: false },
  );

  ResendLink$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(resendLink),
        switchMap((payload) => {
          return this.authService.resendVerificationLink(payload.email).pipe(
            tap((response) => {
              this.snackbarHelper.openSnackBar(this.translate.instant(response.message));
            }),
            catchError((error) => {
              return of(loginFailure({ error: error.error.error }));
            }),
          );
        }),
      ),
    { dispatch: false },
  );

  LoginFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loginFailure),
        tap((payload) => {
          // console.log(source);
        }),
      ),
    { dispatch: false },
  );

  Logout$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(logout),
        tap((payload) => {
          this.authService.clearAuthData();
          this.router.navigateByUrl('/');
        }),
      ),
    { dispatch: false },
  );
}
