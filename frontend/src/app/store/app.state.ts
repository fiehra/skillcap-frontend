import { ActionReducerMap } from '@ngrx/store';
import { AuthState, authenticationReducer } from './authentication.store';

export interface AppState {
  authenticationState: AuthState;
}

export const appReducer: ActionReducerMap<AppState> = {
  authenticationState: authenticationReducer,
};
