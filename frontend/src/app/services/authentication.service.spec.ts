import { fakeAsync, flush, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationService } from './authentication.service';
import { TranslateModule } from '@ngx-translate/core';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of, take } from 'rxjs';
import { HttpClient, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SnackbarHelper } from '../core/helper/snackbar.helper';
import { provideRouter } from '@angular/router';

describe('AuthService', () => {
  let service: AuthenticationService;
  let http: HttpClient;
  let store: MockStore;

  const now = new Date();
  const soon = new Date(now.getMilliseconds() + 100);

  const authDataMock = {
    token: 'jwtTokenXXXXXXX',
    username: 'username',
    userId: 'userId',
    expirationDate: soon,
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
    imports: [
        BrowserAnimationsModule,
        TranslateModule.forRoot({})],
    providers: [
        FormBuilder,
        SnackbarHelper,
        provideRouter([]),
        provideMockStore({
            initialState: {
                authentication: {
                    isAuthenticated: false,
                    error: null,
                },
            },
        }),
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
    ]
}).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.inject(AuthenticationService);
    http = TestBed.inject(HttpClient);
    store = TestBed.inject(MockStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(http).toBeTruthy();
  });

  it('getToken', () => {
    localStorage.setItem('token', 'jwtTokenXXXXXXX');
    const token = service.getToken();
    expect(token).toBe('jwtTokenXXXXXXX');
  });

  it('getUsername', () => {
    localStorage.setItem('username', 'fiehra');
    const username = service.getUsername();
    expect(username).toBe('fiehra');
  });

  it('getUserId', () => {
    localStorage.setItem('userId', 'userIdXXXXXX');
    const userId = service.getUserId();
    expect(userId).toBe('userIdXXXXXX');
  });

  it('getExpirationDate', () => {
    localStorage.setItem('expirationDate', '2022-1-1');
    const expirationDate = service.getExpirationDate();
    expect(expirationDate).toBe('2022-1-1');
  });

  it('signUp', fakeAsync(() => {
    jest.spyOn(http, 'post').mockReturnValue(of({}));
    const response = service
      .signUp('username', 'email@email', 'password')
      .pipe(take(1))
      .subscribe((res) => {
        expect(res).toEqual({});
      });
    expect(http.post).toHaveBeenCalled();
    flush();
  }));

  it('login', fakeAsync(() => {
    jest.spyOn(http, 'post').mockReturnValue(of(authDataMock));
    const response = service
      .login('username', 'password')
      .pipe(take(1))
      .subscribe((res) => {
        expect(res).toEqual(authDataMock);
      });
    expect(http.post).toHaveBeenCalledWith(environment.backendUrl + '/api/auth/login', {
      username: 'username',
      password: 'password',
    });
    flush();
  }));

  it('refreshToken', fakeAsync(() => {
    jest.spyOn(http, 'post').mockReturnValue(of(authDataMock));
    const response = service
      .refreshToken('username', 'expirationDate')
      .pipe(take(1))
      .subscribe((res) => {
        expect(res).toEqual(authDataMock);
      });
    expect(http.post).toHaveBeenCalledWith(
      environment.backendUrl + '/api/auth/refreshToken',
      { username: 'username', expirationDate: 'expirationDate' },
    );
    flush();
  }));

  it('autoLogin', () => {
    const now = new Date();
    const soonMock = new Date(now.getTime() + 10);
    const authDataSoon = {
      username: 'username',
      token: 'jwtTokenXXXXXXX',
      userId: 'userIdXXXXX',
      expirationDate: soonMock.toISOString(),
    };
    service.saveAuthData(authDataSoon.username, authDataSoon.username, authDataSoon.userId, authDataSoon.expirationDate);
    expect(service.getExpirationDate()).toBe(soonMock.toISOString());
    jest.spyOn(store, 'dispatch');
    service.autoLogin();
    expect(store.dispatch).toHaveBeenCalled();
  });

  it('autoLogin no auth data', () => {
    service['clearAuthData']();
    jest.spyOn(store, 'dispatch');
    service.autoLogin();
    expect(store.dispatch).not.toHaveBeenCalled();
  });

  it('autoLogout', fakeAsync(() => {
    jest.useFakeTimers();
    const now = new Date();
    const date = new Date(now.getTime() + 100);
    jest.spyOn(service, 'getExpirationDate').mockReturnValue(date.toISOString());
    jest.spyOn(store, 'dispatch');

    service.autoLogout();

    jest.advanceTimersByTime(150);
    expect(store.dispatch).toHaveBeenCalled();
    expect(service.getExpirationDate).toHaveBeenCalled();
    jest.clearAllTimers();
  }));
});
