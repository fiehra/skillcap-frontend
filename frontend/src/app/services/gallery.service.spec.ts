import { fakeAsync, flush, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { HttpClient, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { SnackbarHelper } from '../core/helper/snackbar.helper';
import { GalleryService } from './gallery.service';

describe('GalleryService', () => {
  let service: GalleryService;
  let http: HttpClient;
  let store: MockStore;

  const now = new Date();
  const soon = new Date(now.getMilliseconds() + 100);

  const authDataMock = {
    userId: 'id',
    email: 'user@email',
    username: 'name',
    role: 'admin',
    token: 'jwtTokenXXXXXXX',
    expirationDate: soon,
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
    imports: [
        BrowserAnimationsModule,
        RouterTestingModule,
        TranslateModule.forRoot({})],
    providers: [
        FormBuilder,
        SnackbarHelper,
        provideMockStore({
            initialState: {
                authentication: {
                    isAuthenticated: false,
                    error: null,
                },
            },
        }),
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
    ]
}).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.inject(GalleryService);
    http = TestBed.inject(HttpClient);
    store = TestBed.inject(MockStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(http).toBeTruthy();
  });
});
