import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthData } from '../models/auth.model';
import { Store } from '@ngrx/store';
import { logout, refreshLogin } from '../store/authentication.store';
import { AppState } from '../store/app.state';
import { SsrHelper } from '../core/helper/ssr.helper';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private ssrHelper: SsrHelper, private http: HttpClient, private store: Store<AppState>) {}

  getToken() {
    return this.ssrHelper.getLocalstorage('token');
  }

  getUsername() {
    return this.ssrHelper.getLocalstorage('username');
  }

  getUserId() {
    return this.ssrHelper.getLocalstorage('userId');
  }

  getExpirationDate() {
    return this.ssrHelper.getLocalstorage('expirationDate');
  }

  clearAuthData() {
    this.ssrHelper.removeLocalstorage('token');
    this.ssrHelper.removeLocalstorage('username');
    this.ssrHelper.removeLocalstorage('userId');
    this.ssrHelper.removeLocalstorage('expirationDate');
  }

  saveAuthData(token: string, username: string, userId: string, expirationDate: string) {
    this.ssrHelper.setLocalstorage('token', token)
    this.ssrHelper.setLocalstorage('username', username)
    this.ssrHelper.setLocalstorage('userId', userId)
    this.ssrHelper.setLocalstorage('expirationDate', expirationDate)
  }

  getLocalAuthdata() {
    const token = this.ssrHelper.getLocalstorage('token');
    const username = this.ssrHelper.getLocalstorage('username');
    const userId = this.ssrHelper.getLocalstorage('userId');
    const expirationDate = this.ssrHelper.getLocalstorage('expirationDate');
    return {
      token: token,
      username: username,
      userId: userId,
      expirationDate: expirationDate,
    };
  }

  signUp(email: string, password: string, username: string): Observable<{}> {
    return this.http.post<{ userId: string; message: string }>(
      environment.backendUrl + '/api/auth/signup',
      {
        email: email,
        username: username,
        password: password,
      },
    );
  }

  login(username: string, password: string): Observable<{ authData: AuthData }> {
    return this.http.post<{ authData: AuthData }>(
      environment.backendUrl + '/api/auth/login',
      {
        username: username,
        password: password,
      },
    );
  }

  refreshToken(username: string, expirationDate: string): Observable<{ authData: AuthData }> {
    return this.http.post<{ authData: AuthData }>(
      environment.backendUrl + '/api/auth/refreshToken',
      { username: username, expirationDate: expirationDate },
    );
  }

  resendVerificationLink(email: string): Observable<{ token: string; message: string }> {
    return this.http.post<{ token: string; message: string }>(
      environment.backendUrl + '/api/verification/resend',
      { email: email },
    );
  }

  autoLogin() {
    if (!this.ssrHelper.getLocalstorage('token')) {
      return;
    } else {
      let expirationDate = this.ssrHelper.getLocalstorage('expirationDate');
      const now = new Date();
      const expirationDateValue = new Date(expirationDate);
      const expiresIn = expirationDateValue.getTime() - now.getTime();
      if (expiresIn > 0) {
        this.store.dispatch(
          refreshLogin({
            expirationDate: this.ssrHelper.getLocalstorage('expirationDate'),
            username: this.ssrHelper.getLocalstorage('username'),
          }),
        );
      } else {
        return;
      }
    }
  }

  autoLogout() {
    const expirationTime = new Date(this.getExpirationDate());
    const now = new Date();
    let expiresIn = expirationTime.getTime() - now.getTime();
    console.log('expires in: ' + expiresIn / 1000 + ' seconds');
    setTimeout(() => {
      this.store.dispatch(logout());
    }, expiresIn);
  }
}
