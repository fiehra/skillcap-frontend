import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Image } from '../models/image.model';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root',
})
export class GalleryService {
  constructor(private http: HttpClient, private authService: AuthenticationService) {}

  fetchImages() {
    return this.http.get<Image[]>(environment.backendUrl + '/gallery/images/fetch', {
      headers: this.setAuthHeaders(),
    });
  }

  addImage(image: Image) {
    return this.http.post<Image>(environment.backendUrl + '/gallery/image/add', image, {
      headers: this.setAuthHeaders(),
    });
  }

  updateImage(image: Image) {
    return this.http.post<Image>(
      environment.backendUrl + '/gallery/image/update',
      image,
      { headers: this.setAuthHeaders() },
    );
  }

  deleteImage(image: Image) {
    return this.http.delete<Image>(
      environment.backendUrl + '/gallery/image/delete' + image.id,
      { headers: this.setAuthHeaders() },
    );
  }

  uploadImage(event: File) {
    const formData = new FormData();
    formData.append('image', event);

    return this.http.post<{ imageUrl: string }>(
      environment.backendUrl + '/gallery/file/add',
      formData,
      { headers: this.setAuthHeaders() },
    );
  }

  setAuthHeaders() {
    const token = this.authService.getToken();
    if (!token) {
      return null;
    } else {
      const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
      return headers;
    }
  }
}
