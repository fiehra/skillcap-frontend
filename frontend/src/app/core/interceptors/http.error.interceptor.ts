import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { SnackbarHelper } from '../helper/snackbar.helper';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(
    private snackbarHelper: SnackbarHelper,
    private translateService: TranslateService,
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        switch (error.status) {
          case 500:
            this.handleError(error);
            break;
          case 400:
            this.handleError(error);
            break;
          case 401:
            this.handleError(error);
            break;
          case 404:
            this.handleError(error);
            break;
          case 0:
            break;
          default:
            this.handleError(error);
            break;
        }
        return throwError(() => error);
      }),
    );
  }

  handleError(error: any) {
    if (!error.error.message) {
      const unknownError = this.translateService.instant('unknownError');
      this.snackbarHelper.openSnackBar(unknownError);
    } else {
      this.snackbarHelper.openSnackBar(error.error.message);
    }
  }
}
