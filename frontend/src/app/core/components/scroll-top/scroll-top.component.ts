import { Component, OnInit, HostListener } from '@angular/core';
import { NgIf } from '@angular/common';
import { SsrHelper } from '../../helper/ssr.helper';

@Component({
    selector: 'scroll-top',
    templateUrl: './scroll-top.component.html',
    styleUrls: ['./scroll-top.component.scss'],
    standalone: true,
    imports: [NgIf],
})
export class ScrollTopComponent implements OnInit {
  showScrollButton: boolean = false;

  constructor(private ssrHelper: SsrHelper) {}

  ngOnInit() {}

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (this.ssrHelper.isBrowser) {
      if (
        (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) > 764
      ) {
        this.showScrollButton = true;
      } else if (
        this.showScrollButton &&
          (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) < 764
      ) {
        this.showScrollButton = false;
      }
    }
  }

  scrollToTop() {
    if (this.ssrHelper.isBrowser) {
      window.scrollTo({top: 0, behavior: 'smooth'});
    }
  }
}
