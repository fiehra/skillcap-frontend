import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrollTopComponent } from './scroll-top.component';

window.scrollTo = jest.fn();

describe('ScrollTopComponent', () => {
  let component: ScrollTopComponent;
  let fixture: ComponentFixture<ScrollTopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    imports: [ScrollTopComponent],
}).compileComponents();

    fixture = TestBed.createComponent(ScrollTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onWindowScroll', () => {
    component.onWindowScroll();
    expect(component.showScrollButton).toBe(false);
    window = Object.assign(window, { pageYOffset: 2000 });
    component.onWindowScroll();
    expect(component.showScrollButton).toBe(true);
  });

  it('onWindowScroll', () => {
    window = Object.assign(window, { pageYOffset: 2000 });
    component.onWindowScroll();
    expect(component.showScrollButton).toBe(true);
    window = Object.assign(window, { pageYOffset: 0 });
    component.onWindowScroll();
    expect(component.showScrollButton).toBe(false);
  });

  it('scrollToTop body scrolltop 2000', () => {
    document.body.scrollTop = 2000;
    component.scrollToTop();
    fixture.detectChanges();
    expect(window.pageYOffset).toBe(0);
  });
});
