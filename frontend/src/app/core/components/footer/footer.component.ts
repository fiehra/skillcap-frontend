import { Component, OnInit } from '@angular/core';
import { VersionHelper } from 'src/app/core/helper/version.helper';
import { TranslateModule } from '@ngx-translate/core';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  standalone: true,
  imports: [RouterLink, TranslateModule],
})
export class FooterComponent implements OnInit {
  version: string;
  constructor(private versionHelper: VersionHelper) {}

  ngOnInit(): void {
    this.version = this.versionHelper.getVersion();
  }
}
