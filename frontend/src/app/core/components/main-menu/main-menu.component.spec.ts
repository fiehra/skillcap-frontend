import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { MainMenuComponent } from './main-menu.component';
import { ActivatedRoute } from '@angular/router';

describe('MainMenuComponent', () => {
  let component: MainMenuComponent;
  let fixture: ComponentFixture<MainMenuComponent>;
  let translateService: TranslateService;
  let store: MockStore;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {

          }
        },
        provideMockStore({
          initialState: {
            authentication: {
              isAuthenticated: false,
              error: null,
            },
          },
        }),
      ],
      imports: [
        NoopAnimationsModule,
        TranslateModule.forRoot({}),
        MainMenuComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MainMenuComponent);
    component = fixture.componentInstance;
    translateService = TestBed.inject(TranslateService);
    store = TestBed.inject(MockStore);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('toggleMenu', () => {
    component.toggleMenu()
    fixture.detectChanges();
    expect(component.menuIsOpen).toBe(true);
    component.toggleMenu()
    fixture.detectChanges();
    expect(component.menuIsOpen).toBe(false);
  });

  it('openMenu', () => {
    component.openMenu()
    fixture.detectChanges();
    expect(component.fabTogglerState).toBe('active');
    expect(component.menuIsOpen).toBe(true);
  });

  it('closeMenu', () => {
    component.closeMenu()
    fixture.detectChanges();
    expect(component.fabTogglerState).toBe('inactive');
    expect(component.menuIsOpen).toBe(false);
    expect(component.menuType).toBe('main');
  });

  it('setMenuType', () => {
    component.setMenuType('language')
    fixture.detectChanges();
    expect(component.menuType).toBe('language');
  });

  it('changeLanguage', () => {
    jest.spyOn(component, 'setCurrentLanguage').mockImplementation();
    jest.spyOn(translateService, 'use').mockImplementation();

    component.changeLanguage('en');
    fixture.detectChanges();
    expect(translateService.use).toHaveBeenCalled();
    expect(component.setCurrentLanguage).toHaveBeenCalledWith('en');
  });

  it('changeLanguage', () => {
    jest.spyOn(store, 'dispatch');
    component.logout();
    fixture.detectChanges();
    expect(store.dispatch).toHaveBeenCalled();
  });

  it('setCurrentLanguage', () => {
    component.setCurrentLanguage('en');
    fixture.detectChanges();
    expect(component.currentLanguage).toBe('english');

    component.setCurrentLanguage('de');
    fixture.detectChanges();
    expect(component.currentLanguage).toBe('deutsch');

    component.setCurrentLanguage('jp');
    fixture.detectChanges();
    expect(component.currentLanguage).toBe('日本語');

    component.setCurrentLanguage('tu');
    fixture.detectChanges();
    expect(component.currentLanguage).toBe('türkçe');
  });
});
