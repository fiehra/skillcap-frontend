import { Component, ElementRef, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { Store } from '@ngrx/store';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { AppState } from 'src/app/store/app.state';
import { logout, selectIsAuthenticated } from 'src/app/store/authentication.store';
import { SkillcapAnimations } from 'src/app/core/helper/animation.helper';
import { NgIf, AsyncPipe } from '@angular/common';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
  animations: SkillcapAnimations,
  standalone: true,
  imports: [
    RouterLink,
    NgIf,
    AsyncPipe,
    TranslateModule,
  ],
})
export class MainMenuComponent implements OnInit, OnDestroy {
  authenticationSub: Subscription;
  isAuthenticated$ = this.store.select(selectIsAuthenticated);
  fabTogglerState = 'inactive';
  menuIsOpen: boolean = false;
  menuType: string = 'main';
  currentLanguage: string;
  constructor(
    private store: Store<AppState>,
    private translateService: TranslateService,
    private renderer: Renderer2,
    private elRef: ElementRef,
  ) {}

  ngOnInit(): void {
    this.setCurrentLanguage(this.translateService.currentLang);
    this.renderer.listen('document', 'click', (event: Event) => {
      if (this.menuIsOpen && !this.elRef.nativeElement.contains(event.target)) {
        this.closeMenu();
      }
    });
  }

  toggleMenu() {
    if (!this.menuIsOpen) {
      this.openMenu()
    } else {
      this.closeMenu()
    }
  }

  openMenu() {
    this.menuIsOpen = true
    this.fabTogglerState = 'active'
  }

  closeMenu() {
    this.setMenuType('main')
    this.menuIsOpen = false
    this.fabTogglerState = 'inactive'
  }

  setMenuType(type: string) {
    this.menuType = type;
  }

  // language swtiching
  changeLanguage(language: string) {
    this.translateService.use(language);
    this.setCurrentLanguage(language);
    this.closeMenu()
  }

  setCurrentLanguage(language: string) {
    if (language === 'en') {
      this.currentLanguage = 'english';
    } else if (language === 'de') {
      this.currentLanguage = 'deutsch';
    } else if (language === 'tu') {
      this.currentLanguage = 'türkçe';
    } else if (language === 'jp') {
      this.currentLanguage = '日本語';
    }
  }

  logout() {
    this.closeMenu()
    this.store.dispatch(logout());
  }

  ngOnDestroy() {
    this.authenticationSub?.unsubscribe();
  }
}
