import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ScrollHelper } from 'src/app/core/helper/scroll.helper';

@Component({
    selector: 'scroll-indicator',
    templateUrl: './scroll-indicator.component.html',
    styleUrls: ['./scroll-indicator.component.scss'],
    standalone: true,
})
export class ScrollIndicatorComponent implements OnInit, OnDestroy {
  public pageScroll!: number;
  public subscription!: Subscription;

  constructor(private scrollHelper: ScrollHelper) {}

  ngOnInit() {
    this.subscription = this.scrollHelper
      .getScrollAsStream()
      .subscribe((percent: number): void => {
        this.pageScroll = percent;
      });
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }
}
