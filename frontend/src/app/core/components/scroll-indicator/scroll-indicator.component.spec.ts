import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { ScrollHelper } from 'src/app/core/helper/scroll.helper';
import { ScrollIndicatorComponent } from './scroll-indicator.component';

describe('ScrollIndicatorComponent', () => {
  let component: ScrollIndicatorComponent;
  let fixture: ComponentFixture<ScrollIndicatorComponent>;
  let scrollHelper: ScrollHelper;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    imports: [ScrollIndicatorComponent],
    providers: [ScrollHelper],
}).compileComponents();

    fixture = TestBed.createComponent(ScrollIndicatorComponent);
    component = fixture.componentInstance;
    scrollHelper = TestBed.inject(ScrollHelper);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    jest.spyOn(scrollHelper, 'getScrollAsStream').mockReturnValue(of(0));
    component.ngOnInit();
    fixture.detectChanges();
    expect(scrollHelper.getScrollAsStream).toHaveBeenCalled();
  });

  it('ngOnDestroy', () => {
    jest.spyOn(component.subscription, 'unsubscribe').mockImplementation();
    component.ngOnDestroy();
    fixture.detectChanges();
    expect(component.subscription.unsubscribe).toHaveBeenCalled();
  });
});
