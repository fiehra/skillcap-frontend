import { TranslateModule } from '@ngx-translate/core';
import {
  ReactiveFormsModule,
  UntypedFormControl,
  Validators,
} from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SkillcapInputComponent } from './skillcap-input.component';
import { FormHelper } from 'src/app/core/helper/form.helper';

@Component({
  selector: 'host-component',
  template: '<skillcap-input [control]="inputControl" [placeholder]="placeholder"></skillcap-input>',
  standalone: true,
  imports: [
    SkillcapInputComponent,
    ReactiveFormsModule
  ],
})
class TestHostComponent {
  placeholder = 'placeholder';
  inputControl = new UntypedFormControl('email');
}

describe('SkillcapInputComponent', () => {
  let component: SkillcapInputComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [FormHelper],
      imports: [
        ReactiveFormsModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({}),
        SkillcapInputComponent,
        TestHostComponent,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('get control and display email error', () => {
    fixture.componentInstance.inputControl = new UntypedFormControl('email', [
      Validators.email,
    ]);
    fixture.detectChanges();
    expect(component.inputControl.hasError('email')).toBeTruthy();
  });
});
