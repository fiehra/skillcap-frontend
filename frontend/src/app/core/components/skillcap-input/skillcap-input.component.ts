import { Component, Input, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormControl } from '@angular/forms';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { FormHelper } from 'src/app/core/helper/form.helper';
import { NgIf } from '@angular/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'skillcap-input',
  templateUrl: './skillcap-input.component.html',
  styleUrls: ['./skillcap-input.component.scss'],
  standalone: true,
  imports: [
    ReactiveFormsModule,
    NgIf,
    TranslateModule,
  ],
})
export class SkillcapInputComponent implements OnInit {
  inputControl: FormControl;
  requiredErrorMessage!: string;
  noValidNumberErrorMessage!: string;

  @Input() placeholder: string = '';
  @Input() type: string = 'text';
  @Input() set control(controlObj: FormControl) {

    this.inputControl = controlObj;
    if (this.placeholder && this.placeholder !== '') {
      this.requiredErrorMessage = this.formHelper.createRequiredErrorMessage(
        this.placeholder,
      );
    }
    this.noValidNumberErrorMessage = this.formHelper.createNoValidNumberErrorMessage();
  }

  get control(): FormControl {
    return this.inputControl;
  }

  constructor(public translateService: TranslateService, public formHelper: FormHelper) {}

  ngOnInit() {
  }
}
