import { TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { FormHelper } from './form.helper';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { HttpClient, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

describe('FormHelper', () => {
  let formHelper: FormHelper;
  let formBuilder: FormBuilder;
  let translateService: TranslateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
    imports: [TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (http: HttpClient) => new TranslateHttpLoader(http, 'assets/i18n/', '.json'),
                deps: [HttpClient],
            },
        })],
    providers: [FormHelper, FormBuilder, provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
}).compileComponents();
  }));

  beforeEach(() => {
    formHelper = TestBed.inject(FormHelper);
    formBuilder = TestBed.inject(FormBuilder);
    translateService = TestBed.inject(TranslateService);
  });

  it('should be created', () => {
    expect(formHelper).toBeTruthy();
  });

  it('check required validator', () => {
    let control = new FormControl('', [Validators.required]);
    expect(formHelper.checkRequiredValidator(control)).toBeTruthy();
    control = new FormControl('', [Validators.email]);
    expect(formHelper.checkRequiredValidator(control)).toBeFalsy();
  });

  it('check required validator return false', () => {
    let control = new FormControl('', []);
    expect(formHelper.checkRequiredValidator(control)).toBeFalsy();
  });

  it('create required error message', () => {
    jest.spyOn(translateService, 'instant');
    formHelper.createRequiredErrorMessage('error');
    expect(translateService.instant).toHaveBeenCalledWith('isRequiredField', {
      fieldName: 'error',
    });
  });

  it('create no valid number errormessage', () => {
    jest.spyOn(translateService, 'instant');
    formHelper.createNoValidNumberErrorMessage();
    expect(translateService.instant).toHaveBeenCalledWith('invalidNumber');
  });

  it('disable controls', () => {
    const formGroup = formBuilder.group({
      field1: ['', []],
      field2: ['', []],
    });
    expect(formGroup.get('field1')?.enabled).toBeTruthy();
    expect(formGroup.get('field2')?.enabled).toBeTruthy();
    formHelper.disableControls(formGroup);
    expect(formGroup.get('field1')?.enabled).toBeFalsy();
    expect(formGroup.get('field2')?.enabled).toBeFalsy();
  });

  it('disable controls by name', () => {
    const formGroup = formBuilder.group({
      field1: [{ value: '', disabled: false }],
      field2: [{ value: '', disabled: false }],
    });
    expect(formGroup.get('field1')?.enabled).toBeTruthy();
    expect(formGroup.get('field2')?.enabled).toBeTruthy();
    formHelper.disableControlsByName(formGroup, ['field2']);
    expect(formGroup.get('field1')?.enabled).toBeTruthy();
    expect(formGroup.get('field2')?.enabled).toBeFalsy();
  });

  it('enable controls', () => {
    const formGroup = formBuilder.group({
      field1: [{ value: '', disabled: true }],
      field2: [{ value: '', disabled: true }],
    });
    expect(formGroup.get('field1')?.enabled).toBeFalsy();
    expect(formGroup.get('field2')?.enabled).toBeFalsy();
    formHelper.enableControls(formGroup);
    expect(formGroup.get('field1')?.enabled).toBeTruthy();
    expect(formGroup.get('field2')?.enabled).toBeTruthy();
  });

  it('enable controls by name', () => {
    const formGroup = formBuilder.group({
      field1: [{ value: '', disabled: true }],
      field2: [{ value: '', disabled: true }],
    });
    expect(formGroup.get('field1')?.enabled).toBeFalsy();
    expect(formGroup.get('field2')?.enabled).toBeFalsy();
    formHelper.enableControlsByName(formGroup, ['field2']);
    expect(formGroup.get('field1')?.enabled).toBeFalsy();
    expect(formGroup.get('field2')?.enabled).toBeTruthy();
  });

  it('create pws not same error message', () => {
    const formGroup = formBuilder.group({
      password: [{ value: '123', disabled: true }],
      passwordConfirmation: [{ value: '456', disabled: true }],
    });
    jest.spyOn(formGroup.controls['passwordConfirmation'], 'setErrors');
    formHelper.checkPasswords(formGroup);
    expect(formGroup.controls['passwordConfirmation'].setErrors).toHaveBeenCalled();
  });

  // it('create pws not same error message', () => {
  //   const formGroup = formBuilder.group(
  //     {
  //       password: ['123'],
  //       passwordConfirmation: ['123'],
  //     },
  //     { validators: [formHelper.checkPasswords] },
  //   );
  // });
});
