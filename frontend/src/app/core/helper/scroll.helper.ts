// Import the core angular services.
import { fromEvent } from 'rxjs';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { SsrHelper } from './ssr.helper';

type Target = Document | Element;

@Injectable({
  providedIn: 'root',
})
export class ScrollHelper {
  constructor(private ssrHelper: SsrHelper) {}

  public getScroll(node: Target = document): number {
    var currentScroll = this.getCurrentScroll(node);
    var maxScroll = this.getMaxScroll(node);
    var percent = currentScroll / Math.max(maxScroll, 1);
    percent = Math.max(percent, 0);
    percent = Math.min(percent, 1);
    return percent * 100;
  }

  public getScrollAsStream(node: Target = this.ssrHelper.isBrowser ? document: null): Observable<number> {
    if (node instanceof Document) {
      var stream = fromEvent(window, 'scroll').pipe(
        map((event: Event): number => {
          return this.getScroll(node);
        }),
      );
    } else {
      var stream = fromEvent(node, 'scroll').pipe(
        map((event: Event): number => {
          return this.getScroll(node);
        }),
      );
    }
    return stream;
  }

  getCurrentScroll(node: Target): number {
    if (node instanceof Document) {
      return window.scrollY;
    } else {
      return node.scrollTop;
    }
  }

  getMaxScroll(node: Target): number {
    if (node instanceof Document) {
      var scrollHeight = Math.max(
        node.body.scrollHeight,
        node.body.offsetHeight,
        node.body.clientHeight,
        node.documentElement.scrollHeight,
        node.documentElement.offsetHeight,
        node.documentElement.clientHeight,
      );
      var clientHeight = node.documentElement.clientHeight;
      return scrollHeight - clientHeight;
    } else {
      return node.scrollHeight - node.clientHeight;
    }
  }
}
