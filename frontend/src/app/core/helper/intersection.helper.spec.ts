import { TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import {} from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IntersectionHelper } from './intersection.helper';

describe('IntersectionHelper', () => {
  let intersectionHelper: IntersectionHelper;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [IntersectionHelper],
      imports: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    intersectionHelper = TestBed.inject(IntersectionHelper);
  });

  it('should be created', () => {
    expect(intersectionHelper).toBeTruthy();
  });
});
