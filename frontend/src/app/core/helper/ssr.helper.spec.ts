import { TestBed } from '@angular/core/testing';
import { DOCUMENT } from '@angular/common';
import { PLATFORM_ID } from '@angular/core';
import { SsrHelper } from './ssr.helper';

window.scrollTo = jest.fn();

describe('SsrHelper', () => {
  let ssrHelper: SsrHelper;
  let mockDocument: any;

  beforeEach(() => {
    mockDocument = {
      defaultView: {
        localStorage: {
          getItem: jest.fn(),
          setItem: jest.fn(),
          removeItem: jest.fn(),
        }
      }
    };
  });

  afterEach(() => {
    TestBed.resetTestingModule();
    jest.restoreAllMocks(); // Reset all mocks after each test
  });

  describe('when in SSR environment', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        providers: [
          SsrHelper,
          { provide: DOCUMENT, useValue: mockDocument },
          { provide: PLATFORM_ID, useValue: 'server' }
        ]
      });
      ssrHelper = TestBed.inject(SsrHelper);
    });

    it('detect non-browser environment', () => {
      expect(ssrHelper.checkIfBrowser()).toBe(false);
    });

    it('scrolltop', () => {
      const scrollToSpy = jest.spyOn(window, 'scrollTo').mockImplementation();
      expect(ssrHelper.isBrowser).toBe(false)
      ssrHelper.scrollTop();
      expect(scrollToSpy).not.toHaveBeenCalled();
    });
  });

  describe('when in browser environment', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        providers: [
          SsrHelper,
          { provide: DOCUMENT, useValue: mockDocument },
          { provide: PLATFORM_ID, useValue: 'browser' }
        ]
      });
      ssrHelper = TestBed.inject(SsrHelper);
    });

    it('detect browser environment', () => {
      expect(ssrHelper.checkIfBrowser()).toBe(true);
    });

    it('getLocalStorage', () => {
      const key = 'testKey';
      ssrHelper.getLocalstorage(key);
      expect(mockDocument.defaultView.localStorage.getItem).toHaveBeenCalledWith(key);
    });

    it('getLocalStorage', () => {
      const key = 'testKey';
      const value = 'testValue';
      ssrHelper.setLocalstorage(key, value);
      expect(mockDocument.defaultView.localStorage.setItem).toHaveBeenCalledWith(key, value);
    });

    it('getLocalStorage', () => {
      const key = 'testKey';
      ssrHelper.removeLocalstorage(key);
      expect(mockDocument.defaultView.localStorage.removeItem).toHaveBeenCalledWith(key);
    });

    it('scrolltop', () => {
      const scrollToSpy = jest.spyOn(window, 'scrollTo').mockImplementation(() => {});
      ssrHelper.scrollTop();
      expect(scrollToSpy).toHaveBeenCalledWith(0, 0);
    });
  });
});

