import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class IntersectionHelper {
  sizeHoverOptions = {
    root: null,
    rootMargin: '0px',
    threshold: 0.9,
  };

  constructor() {}

  // createSizeHoverObserver() {
  //   let observer = new IntersectionObserver(
  //     this.animateSizeOnView,
  //     this.sizeHoverOptions,
  //   );
  //   return observer;
  // }

  // animateSizeOnView(entries) {
  //   // entries : IntersectionObserverEntry[]
  //   // entry :  IntersectionObserverEntry.target : HtmlElement / Element
  //   entries.forEach((entry) => {
  //     if (entry.intersectionRatio > 0.9) {
  //       entry.target.classList.add('sizeHover');
  //     } else {
  //       entry.target.classList.remove('sizeHover');
  //     }
  //   });
  // }
}
