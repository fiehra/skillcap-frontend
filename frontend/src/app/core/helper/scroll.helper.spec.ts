import { TestBed, waitForAsync } from '@angular/core/testing';
import { ScrollHelper } from './scroll.helper';

describe('ScrollHelper', () => {
  let scrollHelper: ScrollHelper;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [ScrollHelper],
      imports: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    scrollHelper = TestBed.inject(ScrollHelper);
  });

  it('should be created', () => {
    expect(scrollHelper).toBeTruthy();
  });

  it('getScroll', () => {
    jest.spyOn(scrollHelper, 'getCurrentScroll').mockImplementation();
    jest.spyOn(scrollHelper, 'getMaxScroll').mockImplementation();
    scrollHelper.getScroll(document);
    expect(scrollHelper.getCurrentScroll).toHaveBeenCalled();
    expect(scrollHelper.getMaxScroll).toHaveBeenCalled();
  });

  //   it('getScrollAsStream node document', () => {
  //     window = Object.assign(window, { pageYOffset: 1000 });
  //     // jest.spyOn(scrollHelper, 'getScroll').mockImplementation();
  //     scrollHelper.getScrollAsStream(document);
  //     // expect(scrollHelper.getScroll).toHaveBeenCalled();
  // });

  // it('getScrollAsStream node element', () => {
  //     window = Object.assign(window, { pageYOffset: 1000 });
  //     scrollHelper.getScrollAsStream(document.body);
  //     expect(scrollHelper.getScroll()).toBe(1100)
  // });

  it('getCurrentScroll document', () => {
    window = Object.assign(window, { pageYOffset: 0 });
    scrollHelper.getCurrentScroll(document);
    expect(window.pageYOffset).toBe(0);
  });

  it('getCurrentScroll document.body', () => {
    window = Object.assign(window, { pageYOffset: 0 });
    scrollHelper.getCurrentScroll(document.body);
    expect(document.body.scrollTop).toBe(0);
  });

  it('getMaxScroll document', () => {
    scrollHelper.getMaxScroll(document);
  });

  it('getMaxScroll document.body', () => {
    scrollHelper.getMaxScroll(document.body);
  });
});
