import { TestBed, waitForAsync } from '@angular/core/testing';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { SnackbarHelper } from './snackbar.helper';

describe('SnackbarHelper', () => {
  let service: SnackbarHelper;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
    imports: [],
    providers: [SnackbarHelper, provideHttpClient(withInterceptorsFromDi())]
}).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.inject(SnackbarHelper);
  });

  it('initialization', () => {
    expect(service).toBeTruthy();
  });

  it('open snack bar', () => {
  });
});
