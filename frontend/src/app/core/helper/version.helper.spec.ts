import { TestBed, waitForAsync } from '@angular/core/testing';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { VersionHelper } from './version.helper';

describe('VersionHelper', () => {
  let service: VersionHelper;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
    imports: [],
    providers: [provideHttpClient(withInterceptorsFromDi())]
}).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.inject(VersionHelper);
  });

  it('initialization', () => {
    expect(service).toBeTruthy();
  });

  it('getVersion', () => {
    expect(service.getVersion()).toBe('2.1.3');
  })

  it('getApiVersion', () => {
    expect(service.getApiVersion()).toBe('1.0.0');
  })
});
