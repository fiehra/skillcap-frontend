import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class VersionHelper {
  version: string = '2.1.3'
  apiVersion: string = '1.0.0'

  constructor() {}

  getVersion() {
    return this.version;
  }

  getApiVersion() {
    return this.apiVersion;
  }
}
