import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, take } from 'rxjs';
import { AppState } from 'src/app/store/app.state';
import { selectIsAuthenticated } from 'src/app/store/authentication.store';
import { Store } from '@ngrx/store';

@Injectable()
export class AuthGuard  {
  isAuthenticated = false;
  constructor(
    private router: Router,
    private store: Store<AppState>,
  ) {}

  canActivate(): boolean | Observable<boolean> | Promise<boolean> {
    this.store
      .select(selectIsAuthenticated)
      .pipe(take(1))
      .subscribe((isAuthenticated) => {
        this.isAuthenticated = isAuthenticated;
        if (!isAuthenticated) {
          this.router.navigate(['/login']);
        }
      });
    return this.isAuthenticated;
  }
}
