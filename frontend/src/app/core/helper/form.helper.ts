import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class FormHelper {
  constructor(private translateService: TranslateService) {}

  checkRequiredValidator(control: FormControl) {
    if (!control.validator) {
      return false;
    } else {
      const validator = control.validator({} as AbstractControl);
      if (validator && validator['required']) {
        return true;
      } else {
        return false;
      }
    }
  }

  createRequiredErrorMessage(fieldName: string) {
    return this.translateService.instant('isRequiredField', {
      fieldName,
    });
  }

  createNoValidNumberErrorMessage(): string {
    return this.translateService.instant('invalidNumber');
  }

  // control handling for forms
  disableControls(formGroup: FormGroup): void {
    Object.keys(formGroup.controls).forEach((key) => {
      formGroup.get(key)?.disable({ emitEvent: false });
    });
  }

  disableControlsByName(formGroup: AbstractControl, controlNames: string[]) {
    for (const controlName of controlNames) {
      formGroup.get(controlName)?.disable({ emitEvent: false });
    }
  }

  enableControls(formGroup: FormGroup): void {
    Object.keys(formGroup.controls).forEach((key) => {
      formGroup.get(key)?.enable({ emitEvent: false });
    });
  }

  enableControlsByName(formGroup: AbstractControl, controlNames: string[]) {
    for (const controlName of controlNames) {
      formGroup.get(controlName)?.enable({ emitEvent: false });
    }
  }

  // password check for signup page
  checkPasswords(formGroup: FormGroup) {
    const pass = formGroup.controls['password'].value;
    const passwordConfirmation = formGroup.controls['passwordConfirmation'].value;
    if (pass && pass !== '' && pass !== passwordConfirmation) {
      formGroup.controls['passwordConfirmation'].setErrors({ pwNotSame: true });
      formGroup.controls['passwordConfirmation'].markAsTouched();
    } else {
      formGroup.controls['passwordConfirmation'].setErrors(null);
      formGroup.controls['passwordConfirmation'].markAsUntouched();
    }
    return null;
  }
}
