import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SsrHelper {
  isBrowser: boolean;
  localstorage: any;

  constructor(@Inject(PLATFORM_ID) private platformId: any, @Inject(DOCUMENT) private document: Document) {
    this.isBrowser = isPlatformBrowser(this.platformId);
    this.localstorage = document.defaultView?.localStorage;
  }

  getLocalstorage(key: string) {
    return this.localstorage?.getItem(key);
  }

  setLocalstorage(key: string, value: string) {
    this.localstorage.setItem(key, value);
  }

  removeLocalstorage(key: string) {
    this.localstorage.removeItem(key);
  }

  checkIfBrowser() {
    return this.isBrowser
  }

  scrollTop() {
    if (!this.isBrowser) {
      return
    } else {
      window.scrollTo(0, 0);
    }
  }

}

