import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { take } from 'rxjs';
import { AppState } from 'src/app/store/app.state';
import { selectToken } from 'src/app/store/authentication.store';

export interface HttpConfig {
  body?: any;
  headers?: HttpHeaders;
  observe?: any;
}

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthenticationService,
    private store: Store<AppState>,
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const authToken = this.store
      .select(selectToken)
      .pipe(take(1))
      .subscribe((token) => {
        return token;
      });
    const authRequest = req.clone({
      headers: req.headers.set('Authorization', 'Bearer ' + authToken),
    });
    return next.handle(authRequest);
  }
}
