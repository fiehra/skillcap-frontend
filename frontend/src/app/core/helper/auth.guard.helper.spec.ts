import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { AuthGuard } from './auth.guard.helper';
import { Router } from '@angular/router';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

describe('AuthGuard', () => {
  let service: AuthGuard;
  let store: MockStore
  let router: Router;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
    imports: [
        BrowserAnimationsModule,
        RouterTestingModule,
        TranslateModule.forRoot({})],
    providers: [
        AuthGuard,
        provideMockStore({
            initialState: {
                authenticationState: {
                    isAuthenticated: false,
                    error: null,
                },
            },
        }),
        provideHttpClient(withInterceptorsFromDi()),
    ]
}).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.inject(AuthGuard);
    store = TestBed.inject(MockStore);
    router = TestBed.inject(Router);
  });

  it('initialization', () => {
    expect(service).toBeTruthy();
  });

  it('canActivate', () => {
    store.setState({
      authenticationState: {
        isAuthenticated: true,
        error: null,
      },
    });
    jest.spyOn(router, 'navigate').mockImplementation();
    const canActivate = service.canActivate();
    expect(canActivate).toBe(true);
    expect(router.navigate).not.toHaveBeenCalled();
  });

  it('canActivate not logged in', () => {
    jest.spyOn(router, 'navigate').mockImplementation();
    const canActivate = service.canActivate();
    expect(canActivate).toBe(false);
    expect(router.navigate).toHaveBeenCalled();
  })
});
