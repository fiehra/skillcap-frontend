// Import the core angular services.
import { Directive } from '@angular/core';
import { ElementRef } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ScrollHelper } from '../helper/scroll.helper';
import { SsrHelper } from '../helper/ssr.helper';

@Directive({
  selector: '[scrollPercentage]',
  outputs: ['scrollPercentageEvent: scrollPercentage'],
})
export class ScrollPercentageDirective implements OnInit, OnDestroy {
  public scrollPercentageEvent: EventEmitter<number>;
  private elementRef: ElementRef;
  private subscription: Subscription | null;

  constructor(
    elementRef: ElementRef,
    private scrollHelper: ScrollHelper,
    private ssrHelper: SsrHelper
  ) {
    this.elementRef = elementRef;
    this.scrollHelper = scrollHelper;
    this.scrollPercentageEvent = new EventEmitter();
    this.subscription = null;
  }

  public ngOnInit(): void {
    if (!this.ssrHelper.isBrowser) {
      return
    } else {
      console.log(this.ssrHelper.isBrowser)
      this.subscription = this.scrollHelper
        .getScrollAsStream(this.elementRef.nativeElement)
        .subscribe((percent: number): void => {
          this.scrollPercentageEvent.next(percent);
        });
    }
  }

  public ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }
}
