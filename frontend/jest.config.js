module.exports = {
  preset: 'jest-preset-angular',
  setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
  collectCoverage: true,
  coverageDirectory: '<rootDir>/coverage/',
  modulePaths: ['<rootDir>'],
};
