/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,ts}'],
  theme: {
    colors: {
      'primary': '#72fa03',
      'primary-hover': '#aafd66',
      'primary-darker': '#50b400',
      'secondary': '#b5213e',
      'secondary-hover': '#d13f54',
      'background': '#2b2b2b',
      'background-shadow': '#111',
      'primary-font': '#E6EFEC',
      'alt-font': '#111',
      'transparent': '#00000066',
    },
    extend: {},
  },
  plugins: [],
};
